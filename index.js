// To create a class in JS, we use the class keyword and {}
// Naming convention for classes begins with uppercase characters
/*
	Class Syntax:
	class ClassName {
		properties
	}
*/

class Dog {
	// We add constructor method to a class to be able to initialize values upon intantiations of an object from a class
	constructor(name, breed, age){
		this.name = name;
		this.breed = breed;
		this.age = age * 7;
	}
}

// Intantiation - process of creating objects
// an object created class is called an instance

let dog1 = new Dog("Puchi", "Golden retriever", 0.6);
console.log(dog1);


/*	Activity #3:

     1. Should class methods be included in the class constructor?
        Answer: No


    2. Can class methods be separated by commas?
        Answer: No


    3. Can we update an object’s properties via dot notation?
        Answer: Yes


    4. What do you call the methods used to regulate access to an object’s properties?
        Answer: class methods


    5. What does a method need to return in order for it to be chainable?
        Answer: this keyword
*/

// MINI-ACTIVITY: 
class Person {
	constructor(name, age, nationality, address){
		this.name = name;
		if(typeof age === 'number' && age >= 18){
			this.age = age;
		}
		else {
			this.age = undefined;
		}
		this.nationality = nationality;
		this.address = address;
	}
	// 1. Modify the class Person and add the following methods:
	//     a. greet method- the person should be able to greet a certain message
	//     b. introduce method- person should be be able to state their own name
	//     c. change address- person should be able to update their address and send a message where he now lives
	// 2. All methods should be chainable. 
	// 3. Refactor the age by having a condition in which the age should only take in a number data type and a legal age of 18, otherwise undefined
	// 4. Send a screenshot of your outputs in Hangouts and link it in Boodle
	greet(){
		console.log(`Good day beautiful people!`);
		return this;
	}

	introduce(){
		console.log(`Hi! I am ${this.name}`);
		return this;
	}

	changeAddress(newAddress){
		this.addres = newAddress;
		console.log(`${this.name} now lives in ${newAddress}`);
		return this;
	}
}

// Instantiate a person object
let person1 = new Person ("Lisa", 16, "Thai", "Bangkok, Thailand");
console.log(person1);

let person2 = new Person ("Jennie", 26, "Korean", "Seoul, Korea");
console.log(person2);


/*	MINI ACTIVITY

	1. What is the blueprint where objects are created from?
        Answer: class


    2. What is the naming convention applied to classes?
        Answer: Nouns, first letter in uppercase


    3. What keyword do we use to create objects from a class?
        Answer: new


    4. What is the technical term for creating an object from a class?
        Answer: instantiation


    5. What class method dictates HOW objects will be created from that class?
    	Answer: constructor
*/

class Student {
	constructor(name, email, grades){
		this.name = name;
		this.email = email;

		// check first if the array has 4 elements (validation)
		if(grades.length === 4){
			if(grades.every(grade => grade >= 0 && grade <= 100)){
				this.grades = grades;
			}
			else {
				this.grades = undefined;
			}
		}
		else {
			this.grades = undefined;
		}
		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;
	}

	// class methods - would be common to all instances.
	// make sure they are not separated by a comma
	login(){
		console.log(`${this.email} has logged in`);
		return this;
	}

	logout(email){
		console.log(`${this.email} has logged out`);
		return this;
	}

	listGrades(grades) {
		this.grades.forEach(grades => {
			console.log(`${this.name}'s quarterly grade average are ${this.grades}`);
		});
		return this;
	}

	computeAverage(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve = sum / 4;
		return this;
	}

	willPass(){
		this.passed = this.computeAverage().gradeAve >= 85 ? true: false;
		return this;
	}

	willPassWithHonors(){
		if (this.passed) {
			if(this.gradeAve >= 90){
				this.passedWithHonors = true;
			}
			else{
				this.passedWithHonors = false;
			}
		}
		else {
			this.passedWithHonors = false;
		}
		return this;
	}
}

// Instantiate all students from s2 using Student class
let student1 = new Student ("Tony", "starksindustries@mail.com", [89, 84, 78, 88]);
let student2 = new Student ("Peter", "spideyman@mail.com", [78, 82, 79, 85]);
let student3 = new Student ("Wanda", "scarlettMaximoff@mail.com", [87, 89, 91, 93]);
let student4 = new Student ("Steve", "captainRogers@mail.com", [91, 89, 92, 93]);
